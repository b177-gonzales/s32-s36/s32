// imports the User model
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

// User authentication (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}
			// Passwords do not match
			else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Controller for enrolling the user to a specific course

// Async = use if ever you want certain functions magsabay ng executions
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {

	
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Save the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else {
				return true;
			}
		})
	})
	// Add the user Id in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then (course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course info in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Add condition that will check if the user and course documents have been updated
	// User enrollment is successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	// Otherwise if we have user enrollment failure
	else{
		return false;
	}
}
